<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chat extends Model
{
    public function user2(){
        return $this->belongsTo('App\User');
    }
}