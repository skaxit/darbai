<?php

namespace App\Http\Controllers\Auth;


use App\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Http\Controllers\Controller;


class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    //use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware($this->guestMiddleware(), ['except' => 'logout']);
      //  $this->middleware('guest', ['except' => 'getLogout']);
     $this->middleware('jwt.auth', ['except' => ['authenticate', 'create']]);
    }

    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return $this->response->errorUnauthorized();
            }}
        catch
            (JWTException $ex){
                return $this->response->errorInternal();
            }
            return $this->response->array(compact('token'))->setStatusCode(200);

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(Request $request)
    {
      //  $credentials = $request->only('email', 'password', 'name');

        return User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => bcrypt($request['password']),
        ]);
    }
    public function getUser(){
        $user = JWTAuth::parseToken()->authenticate();
        $userId = $user->id;
        return $userId;

    }
    public function index(){
       /* $user = JWTAuth::parseToken()->toUser();
        try{
            if(!$user){
                return $this->response->errorNotFound('User not found');
            }
        }catch (\Tymon\JWTAuth\Exceptions\JWTException $ex) {
            return $this->response->error('Somethin went wrong');
        }
        return $this->response->array(compact('user'))->setStatusCode(200);*/
        $users = User::all();

        return $users;
    }
    public function getToken(){
        $token = JWTAuth::getToken();
        if(!$token){
            return $this->response->errorUnauthorized('Token invalid');
        }
        try{
            $refreshToken = JWTAuth::refresh($token);
        }catch(JWTException $ex){
            $this->response->error('Something went wrong');
        }
        return $this->response->array(compact('refreshToken'));
    }
    public function destroy(){
        $user = JWTAuth::parseToken()->authenticate();
        if($user){
            $user->delete();
        }
    }
}
