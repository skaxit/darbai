<?php
namespace App\Http\Controllers;

use Tymon\JWTAuth\Facades\JWTAuth;
use App\Chat;
use App\User;
use Illuminate\Http\Request;

class ChatController extends Controller
{
    public function index(){
        return Chat::all();
    }
    public function createChat(Request $request){
        $chat = new Chat();
        $chat->body = $request['body'];
        $user = JWTAuth::parseToken()->authenticate();
        $chat->user_id =$user->id;
        $chat->user_name =$user->name;
        $chat->save();
        return $chat->user_id;
    }
    public function destroy(Request $request)
    {

        $parameters =  $request->only('id');
        return Chat::destroy($parameters);


    }
    public function whosPost(Request $request){
        $request['user_id'];

    }
    public function edit(Request $request){


    }
}