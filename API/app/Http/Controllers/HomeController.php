<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Permission;
//use JWTAuth;

class HomeController extends Controller
{

    public function index()
    {

        foreach (User::all() as $user){
            echo "<p>$user->id $user->name $user->email</p>";
        }


    }
    public function attachUserRole($userId, $role){
        $user = User::find($userId);
        $roleId = Role::where('name', $role)->first();
        $user -> roles() -> attach($roleId->id);

        return $user;
    }
    public function getUserRole($userId)
    {
        if (!User::find($userId)->roles->isEmpty()) {
            return $userrole = User::find($userId)->roles[0]->pivot->role_id;
        } else return '0';
    }

        public function destroy(Request $request)
    {
       // return $request;
        $parameters =  $request->only('id');
    return User::destroy($parameters);


    }
    public function attachPermission(Request $request){


        $parameters =  $request->only('permission', 'role');
        $permissionParam = $parameters['permission'];

        $roleParam=$parameters['role'];


        $role = Role::where('name', $roleParam)->first();

        $permission = Permission::where('name', $permissionParam)->first();

        $role->attachPermission($permission);

       return $this->response->created();
    }
}
