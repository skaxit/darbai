<?php

namespace App\Http\Controllers;

//use Dingo\Api\Auth\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests;
use App\webInformation;

use App\Http\Controllers\Controller;

use App\User;

class UserController extends Controller
{
    public function checkAuth(Request $request)
    {
        // setting the credentials array

        $credentials = [
            'email' => $request->input('email'),
            'password' => $request->input('password'),
        ];
        // if the credentials are wrong
        if (!Auth::attempt($credentials)) {
            return response('Username password does not match', 403);
        }
        return response(Auth::user(), 201);
    }

    public function index()
    {
        return view('signUp');
    }
    public function index2()
    {
        return view('signIn');
    }
    public function SignUp(Request $request){

        $this->validate($request, [
            'email' => 'required|email|unique:users',
            'name' => 'required|max:12',
            'password' => 'required|min:4'
        ]);
        $email = $request['email'];
        $password = bcrypt($request['password']);
        $name = $request['name'];

        $user = new User();
        $user->name = $name;
        $user->email = $email;
        $user->password = $password;
        $user->save();
        Auth::login($user);
            return view('welcome');
    }
    public function SignIn(Request $reqeust){
       if(Auth::attempt(['email'=> $reqeust['email'], 'password'=> $reqeust['password']])){
           return redirect()->route('webapi');
       }
       return view('welcome');
    }
}
