<?php
//use Illuminate\Routing\Route;

Route::post('auth', 'UserController@checkAuth');
Route::resource('user', 'UserController');
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$api=app('Dingo\Api\Routing\Router');


/*Route::get('/kl', function () {
    return view('master');
});
Route::group(['middleware'=>'web'], function(){
    Route::get('/signup', 'UserController@index');
    Route::get('/signin', 'UserController@index2');
   Route::post('/signup',[
        'uses' => 'UserController@SignUp',
        'as'=>'signup'
    ]);
    Route::post('/signin',[
        'uses' => 'UserController@SignIn',
        'as'=>'signin'
    ]);
    Route::get('/webapi',[
        'uses'=>'WebInformationController@index',
        'as'=>'webapi',
        'middleware' => 'auth'
        ]);
});
*/
Route::get('/', function () {
    return view('master');
});
$api->version('v1', function($api) {

    $api->post('authenticate','App\Http\Controllers\Auth\AuthController@authenticate');
    $api->post('create','App\Http\Controllers\Auth\AuthController@create');

});
$api->version('v1', ['middleware'=>'api.auth'], function($api){
    $api->get('users', 'App\Http\Controllers\Auth\AuthController@index');
    $api->get('getuser', 'App\Http\Controllers\Auth\AuthController@getUser');
    $api->get('token', 'App\Http\Controllers\Auth\AuthController@getToken');
    $api->post('delete', 'App\Http\Controllers\HomeController@destroy');
    $api->get('users/{user_id}/roles', 'App\Http\Controllers\HomeController@getUserRole');
    $api->post('role/permission/add', 'App\Http\Controllers\HomeController@attachPermission');
    $api->get('users/{user_id}/roles/{role_name}', 'App\Http\Controllers\HomeController@attachUserRole');
    $api->post('createchat','App\Http\Controllers\ChatController@createChat');
    $api->get('chat','App\Http\Controllers\ChatController@index');
    $api->post('chat/delete','App\Http\Controllers\ChatController@destroy');
});

