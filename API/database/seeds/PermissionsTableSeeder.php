<?php

use Illuminate\Database\Seeder;
use App\Permission;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $createInvoice = new Permission();
        $createInvoice -> name =  'create-invoice';
        $createInvoice -> display_name = 'create Invoices';
        $createInvoice -> description = 'create new invoices';
        $createInvoice -> save();

        $editInvoice = new Permission();
        $editInvoice -> name =  'Edit-invoice';
        $editInvoice -> display_name = 'Edit Invoices';
        $editInvoice -> description = 'Edit existing invoices';
        $editInvoice -> save();

        $deleteInvoice = new Permission();
        $deleteInvoice -> name =  'Delete-invoice';
        $deleteInvoice -> display_name = 'Delete Invoices';
        $deleteInvoice -> description = 'Delete existing invoices';
        $deleteInvoice -> save();
    }
}
