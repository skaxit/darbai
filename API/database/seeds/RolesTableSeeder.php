<?php

use App\Role;
use Illuminate\Database\Seeder;


class RolesTableSeeder extends Seeder
{
    public function run()
    {
        $owner = new Role();
        $owner->name = 'owner';
        $owner->display_name = 'Product Owner';
        $owner->description = 'Product owner is the oner of give project';
        $owner->save();

        $owner = new Role();
        $owner->name = 'admin';
        $owner->display_name = 'Admin User';
        $owner->description = 'Product owner is the oner of give project';
        $owner->save();
        // TestDummy::times(20)->create('App\Post');
    }
}
