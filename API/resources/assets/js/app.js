var myApp = angular.module('myApp', ['ngRoute', 'ngCookies', 'satellizer']);

myApp.config(['$routeProvider', '$locationProvider', '$authProvider',
    function($routeProvider, $locationProvider, $authProvider) {
        $authProvider.loginUrl  = 'http://localhost:8000/api/authenticate';
        $routeProvider.when('/', {
            templateUrl: 'templates/users/login.html',
            controller: 'userController'
        });
        $routeProvider.when('/dashboard',{
            templateUrl: 'templates/users/dashboard.html',
            controller: 'userController'
        });
        $routeProvider.when('/logout',{
            templateUrl: 'templates/users/logout.html',
            controller: 'userController'
        });
        $routeProvider.when('/signup',{
            templateUrl: 'templates/users/signup.html',
            controller: 'userController'
        });
        $routeProvider.when('/userlist',{
            templateUrl: 'templates/users/users.html',
            controller: 'userController'
        });
        $routeProvider.when('/edit',{
            templateUrl: 'templates/users/edit.html',
            controller: 'userController'
        });

        $routeProvider.otherwise('/');

    }
]);
myApp.factory('Users', ['$resource',
    function($resource) {
        return $resource('users', {}, {
            update: {
                method: 'PUT'
            },
            remove: {
                method: 'DELETE',
                url: 'users/:id',
                params: {id: '@_id'}
            }
        });
    }
]);