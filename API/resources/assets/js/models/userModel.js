myApp.factory('userModel',['$http', function($http){
    var userModel={};
    userModel.doLogin = function(loginForm){
       return $http({
            header: {
                'Content-Type': 'application/json'
            },
            url: baseUrl + 'api/authenticate',
            method: "POST",
            data: {
                email: loginForm.email,
                password: loginForm.password
            }
        }).success(function (response){
            console.log(response);
        }).error(function(data, status, headers){
            console.log(data, status, headers);
            alert('Invalid username or password');
        });
    };
    userModel.doSignUp = function(signUpForm){
        return $http({
            header: {
                'Content-Type': 'application/json'
            },
            url: baseUrl + 'api/create',
            method: "POST",
            data: {
                name: signUpForm.name,
                email: signUpForm.email,
                password: signUpForm.password
            }
        }).success(function (response){
            console.log(response);
        }).error(function(data, status, headers){
            console.log(data, status, headers);
            alert('There is an account that name');
        });
    };
    return userModel;
}]);