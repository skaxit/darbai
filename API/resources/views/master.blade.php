<!DOCTYPE html>
<html ng-app="myApp">
<head>
    <title>bullshit</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{ URL::to('src/css/main.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <script>var baseUrl="{{url('/')}}/";</script>
</head>
<body>
<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand">Blogsters United</a>
        </div>
        <ul class="nav navbar-nav">
            <li class="active"><a href="http://localhost:8000/#/dashboard">Home</a></li>
            <!--<li><a ng-href='#here' ng-click='getUserRole()'>User List</a></li>-->
        </ul>
        <ul class="nav navbar-nav navbar-right">

            <li><a href="http://localhost:8000/#/signup"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
            <li><a href="http://localhost:8000/#/login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
            <li><a href="http://localhost:8000/#/logout"><span class="glyphicon glyphicon-log-out"></span> Log Out</a></li>
        </ul>
    </div>
</nav>
    <div class="container">
        <div ng-view></div>
    </div>
    <script type="text/javascript" src="{{asset('bower_components/angular/angular.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('bower_components/angular-route/angular-route.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('bower_components/satellizer/dist/satellizer.js')}}"></script>
    <script type="text/javascript" src="{{asset('bower_components/angular-cookies/angular-cookies.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/models.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/controllers.js')}}"></script>

</body>
</html>