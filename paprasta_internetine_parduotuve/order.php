<?php
include("include/session.php");
//Iš pradžių aprašomos funkcijos, po to jos naudojamos.

/**
 * displayUsers - Displays the users database table in
 * a nicely formatted html table.
 */

//Jei prisijunges Administratorius ar Valdytojas vykdomas operacija3 kodas
if ($session->logged_in && ($session->isAdmin() || $session->isManager())) {
    ?>  


    <html>  
       
        <head>  
            <meta http-equiv="X-UA-Compatible" content="IE=9; text/html; charset=utf-8"/> 
            <title>Užsakymo forma</title>
            <link href="include/styles.css" rel="stylesheet" type="text/css" />
        </head>
        <body>   
            <table class="center"><tr><td>
                        <img src="pictures/main.png"/>
                    </td></tr><tr><td> 
                        <?
                        include("include/meniu.php");
                        ?>                   
                        <table style="border-width: 2px; border-style: dotted;"><tr><td>
                                    Atgal į [<a href="index.php">Pradžia</a>]
                                </td></tr></table>               
                        <br> 
                        
                                      <?php
                        /**
                         * The user has submitted the registration form and the
                         * results have been processed.
                         */ if (isset($_SESSION['regsuccess'])) {
                            /* Registracija sėkminga */
                            if ($_SESSION['regsuccess']) {
                                echo "<p>Ačiū, <b>" . $_SESSION['reguname'] . "</b>, kad apsipirkinėjote pas mus. Toliau sekti užsakymo busena galite Mano Paskyra</p><br>";
                            while (list ($key, $val) = each ($_SESSION['cart'])) { 
                            unset($_SESSION['cart'][$key]);}}
                            /* Registracija nesėkminga */ else {
                                echo "<p>Atsiprašome, bet užsakymo <b>" . $_SESSION['reguname'] . "</b>, "
                                . " nepavyko atlikti bandykite dar kartą.<br>Bandykite vėliau.</p>";
                            }
                            unset($_SESSION['regsuccess']);
                            unset($_SESSION['reguname']);
                        }
                        /**
                         * The user has not filled out the registration form yet.
                         * Below is the page with the sign-up form, the names
                         * of the input fields are important and should not
                         * be changed.
                         */ else {
                            ?>
                                <?
                                if ($form->num_errors > 0) {
                                    echo "<font size=\"3\" color=\"#ff0000\">Klaidų: " . $form->num_errors . "</font>";
                                }
                                
                                ?> 
                 <div align="center">
                   
                            <h1>Užpildykite reikiamus duomenis</h1>
                           <table><tr><td>                     
                             <form action="process.php" method ="post" enctype="multipart/form-data">
                            <fieldset>
                             <legend>Užpildyti lentele:</legend>
                            <p>Vardas:<br>
                            <input type=text" name="var" size="30" value="<? echo $form->value("var"); ?>"/><br><? echo $form->error("var"); ?>
                            </p>
                            <p>Pavarde:<br>
                            <input type=text" name="pav" size="30" value="<? echo $form->value("pav"); ?>"/><br><? echo $form->error("pav"); ?>
                            </p>
                            <p>Telefono numeris:<br>
                            <input type=text" name="tel" size="30" value="<? echo $form->value("tel"); ?>"/><br><? echo $form->error("tel"); ?>
                            </p>
                            <p>Miestas:<br>
                            <input type=text" name="mie" size="30" value="<? echo $form->value("mie"); ?>"/><br><? echo $form->error("mie"); ?>
                            </p>
                            <p>Adresas:<br>
                            <input type=text" name="adr" size="30" value="<? echo $form->value("adr"); ?>"/><br><? echo $form->error("adr"); ?>
                            </p>
                            <p>ZIP kodas:<br>
                            <input type=text" name="zip" size="30" value="<? echo $form->value("zip"); ?>"/><br><? echo $form->error("zip"); ?>
                            </p>
                            <p>Komentaras:<br>
                            <input type=text" name="kom" size="30" value="<? echo $form->value("kom"); ?>"/><br><? echo $form->error("kom"); ?>
                            </p>
                            <p>
                                
                    
                              <p>Pasirinkite mokėjimo būda:<br> 
                                  </p>
                              <input type="radio" name="bankas" value="seb">SebBank 
                              <input type="radio" name="bankas" value="swed">SwedBank 
                              <input type="radio" name="bankas" value="dnb">DNB bankas 
                              <br>
                              <?php echo $form->error("bankas"); 
                                
                          ?>
                            </p>
                            <p><br>
                          <input type="submit" name="subuz" value="Patvirtinti" />
                          </p>
                          </fieldset>
                        
                        </form>
                                   </td></tr></table>  
 </div>
                      

                                           
                        </div> 
                        <br>                         
                <tr><td>
                        <?
                        }
                        include("include/footer.php");
                        ?>
                    </td></tr>                       
            </table>     
        </body>
    </html>
    <?
    //Jei vartotojas neprisijungęs arba prisijunges, bet ne Administratorius 
    //ar ne Valdytojas - užkraunamas pradinis puslapis   
} else {
    header("Location: index.php");
}
?>

